
// DATABASE
const databaseName = "tradingbotbs_db"

const ethTableName = "eth_hourly_ticker_tb"
const btcTableName = "btc_hourly_ticker_tb"
const xrpTableName = "xrp_hourly_ticker_tb"
const ltcTableName = "ltc_hourly_ticker_tb"
const bchTableName = "bch_hourly_ticker_tb"

const maEthTableName  = "moving_averages_eth_tb"
const maBtcTableName  = "moving_averages_btc_tb"
const maXrpTableName  = "moving_averages_xrp_tb"
const maLtcTableName  = "moving_averages_ltc_tb"
const maBchTableName  = "moving_averages_bch_tb"

const taEthTableName  = "trading_actions_eth_tb"
const taBtcTableName  = "trading_actions_btc_tb"
const taXrpTableName  = "trading_actions_xrp_tb"
const taLtcTableName  = "trading_actions_ltc_tb"
const taBchTableName  = "trading_actions_bch_tb"

// CURRENCY PAIRS
const eth = "etheur"
const bch = "bcheur"
const btc = "btceur"
const xrp = "xrpeur"
const ltc = "ltceur"

// MOVIGN AVERAGES 
const SMALength = 5
const EMA1Length = 8
const EMA2Length = 13

// REQUESTS
function hourlyTickerRequestURL(currencyPair) {
    return `https://www.bitstamp.net/api/v2/ticker_hour/${currencyPair}/`
}

function balanceRequestUrl() {
    return 'https://www.bitstamp.net/api/v2/balance/'
}

function sellMarketURL(currency) {
    return `https://www.bitstamp.net/api/v2/sell/market/${currency}eur/`
}

function buyMarketURL(currency) {
    return `https://www.bitstamp.net/api/v2/buy/market/${currency}eur/`
}


module.exports = {

    databaseName,

    ethTableName,
    btcTableName,
    xrpTableName,
    ltcTableName,
    bchTableName,
    
    maEthTableName,
    maBtcTableName,
    maXrpTableName,
    maLtcTableName,
    maBchTableName,

    taEthTableName,
    taBtcTableName,
    taXrpTableName,
    taLtcTableName,
    taBchTableName,

    SMALength,
    EMA1Length,
    EMA2Length,

    hourlyTickerRequestURL,
    balanceRequestUrl,
    sellMarketURL,
    buyMarketURL,

    eth,
    bch,
    btc,
    xrp,
    ltc
}