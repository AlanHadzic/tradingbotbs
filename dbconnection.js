require('dotenv').config()
const mysql = require('mysql');
const connection = mysql.createConnection({
    host     : process.env.DATABASE_HOST,
    user     : process.env.DATABASE_USER,
    password : process.env.DATABASE_PASSWORD
});

connection.connect(function(err) {
    if (err) throw err;
});

module.exports = connection;