const { createLogger, format, transports, winston } = require('winston');
const { combine, timestamp, label, printf } = format;

const myFormat = printf(info => {
  return `${info.timestamp} [${info.label}] ${info.level}: ${info.message}`;
});

const logger = createLogger({
  format: combine(
    label({ label: 'TB_BS' }),
    timestamp(),
    myFormat
  ),
  transports: [new transports.Console(),
               new transports.File({filename: './log/application.log'})]
});

module.exports = {
    log: logger
}