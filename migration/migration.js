const migration = require('../models/migration_model')
const constants = require('../constants')
const logger = require('../log/log')

async function executeMigration(){
   
    // DATABASE
    try{
        var data = await migration.Queries.createDatabase(constants.databaseName)
        logger.log.info(`Created Database: ${constants.databaseName}`)
    }catch(err) {
        logger.log.error(`Creating Database: ${err}`)
    }
    try {
        // DATA VALUES
        // BTC
        data = await migration.Queries.dropTable(constants.databaseName, constants.btcTableName)
        data = await migration.Queries.createTableHourlyTicker(constants.databaseName, constants.btcTableName)
        logger.log.info(`Created Table: ${constants.btcTableName}`)
        
        // ETH
        data = await migration.Queries.dropTable(constants.databaseName, constants.ethTableName)
        data = await migration.Queries.createTableHourlyTicker(constants.databaseName, constants.ethTableName)
        logger.log.info(`Created Table: ${constants.ethTableName}`)
        
        // XRP
        data = await migration.Queries.dropTable(constants.databaseName, constants.xrpTableName)
        data = await migration.Queries.createTableHourlyTicker(constants.databaseName, constants.xrpTableName)
        logger.log.info(`Created Table: ${constants.xrpTableName}`)

        // LTC
        data = await migration.Queries.dropTable(constants.databaseName, constants.ltcTableName)
        data = await migration.Queries.createTableHourlyTicker(constants.databaseName, constants.ltcTableName)
        logger.log.info(`Created Table: ${constants.ltcTableName}`)
        
        // BCH
        data = await migration.Queries.dropTable(constants.databaseName, constants.bchTableName)
        data = await migration.Queries.createTableHourlyTicker(constants.databaseName, constants.bchTableName)
        logger.log.info(`Created Table: ${constants.bchTableName}`)
        
        // MOVING AVERAGES
        // BTC
        data = await migration.Queries.dropTable(constants.databaseName, constants.maBtcTableName)
        data = await migration.Queries.createTableMovingAverages(constants.databaseName, constants.maBtcTableName)
        logger.log.info(`Created Table: ${constants.maBtcTableName}`)

        // ETH
        data = await migration.Queries.dropTable(constants.databaseName, constants.maEthTableName)
        data = await migration.Queries.createTableMovingAverages(constants.databaseName, constants.maEthTableName)
        logger.log.info(`Created Table: ${constants.maEthTableName}`)

        // XRP
        data = await migration.Queries.dropTable(constants.databaseName, constants.maXrpTableName)
        data = await migration.Queries.createTableMovingAverages(constants.databaseName, constants.maXrpTableName)
        logger.log.info(`Created Table: ${constants.maXrpTableName}`)

        // LTC
        data = await migration.Queries.dropTable(constants.databaseName, constants.maLtcTableName)
        data = await migration.Queries.createTableMovingAverages(constants.databaseName, constants.maLtcTableName)
        logger.log.info(`Created Table: ${constants.maLtcTableName}`)

        // BCH
        data = await migration.Queries.dropTable(constants.databaseName, constants.maBchTableName)
        data = await migration.Queries.createTableMovingAverages(constants.databaseName, constants.maBchTableName)
        logger.log.info(`Created Table: ${constants.maBchTableName}`)

        // TRADING ACTIONS
        // BTC
        data = await migration.Queries.dropTable(constants.databaseName, constants.taBtcTableName)
        data = await migration.Queries.createTableTradingActions(constants.databaseName, constants.taBtcTableName)
        logger.log.info(`Created Table: ${constants.taBtcTableName}`)

        // ETH
        data = await migration.Queries.dropTable(constants.databaseName, constants.taEthTableName)
        data = await migration.Queries.createTableTradingActions(constants.databaseName, constants.taEthTableName)
        logger.log.info(`Created Table: ${constants.taEthTableName}`)

        // XRP
        data = await migration.Queries.dropTable(constants.databaseName, constants.taXrpTableName)
        data = await migration.Queries.createTableTradingActions(constants.databaseName, constants.taXrpTableName)
        logger.log.info(`Created Table: ${constants.taXrpTableName}`)

        // LTC
        data = await migration.Queries.dropTable(constants.databaseName, constants.taLtcTableName)
        data = await migration.Queries.createTableTradingActions(constants.databaseName, constants.taLtcTableName)
        logger.log.info(`Created Table: ${constants.taLtcTableName}`)

        // BCH
        data = await migration.Queries.dropTable(constants.databaseName, constants.taBchTableName)
        data = await migration.Queries.createTableTradingActions(constants.databaseName, constants.taBchTableName)
        logger.log.info(`Created Table: ${constants.taBchTableName}`)

        logger.log.info(`Migration process completed successfully`)
        
    } catch(err) {
        logger.log.error(`Creating Table: ${err}`)
    }

    process.exit(0)
}

executeMigration()