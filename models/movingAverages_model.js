const db = require('../dbconnection')
const moment = require('moment')
const constants = require('../constants')

const Queries = {
    getLastNrows: function(tableName, n) {
        return new Promise(function(resolve, reject){
            let queryString = `SELECT * FROM (
                SELECT * FROM  \`${constants.databaseName}\`.\`${tableName}\` 
                ORDER BY id DESC LIMIT ${n}) 
                sub ORDER BY id ASC`

            db.query(queryString, function(err, rows, fields){
                if(err){
                    reject(err)
                }
                resolve(rows)
            })
        })
    },
    insertMovingAverages: function(tableName, timestamp, sma, ema1, ema2) {
        return new Promise(function(resolve, reject){
            let queryString = `INSERT INTO \`${constants.databaseName}\`.\`${tableName}\`
                (\`timestamp\`, \`sma\`, \`ema1\`, \`ema2\`)
                VALUES ('${timestamp}', ${sma}, ${ema1}, ${ema2})`

            db.query(queryString, function(err, rows, fields){
                if(err){
                    reject(err)
                }
                resolve(rows)
            })
        })
    }
} 

module.exports = {
    Queries
}