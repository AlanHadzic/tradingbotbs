const db = require('../dbconnection')
const moment = require('moment')
const constants = require('../constants')

const Queries = {
    getLastTradingAction: function(tableName) {
        return new Promise(function(resolve, reject){
            let queryString = `SELECT * FROM (
                SELECT * FROM  \`${constants.databaseName}\`.\`${tableName}\` 
                ORDER BY id DESC LIMIT 1) 
                sub ORDER BY id ASC`

            db.query(queryString, function(err, rows, fields){
                if(err){
                    reject(err)
                }
                resolve(rows)
            })
        })
    },
    insertTradingAction: function(tableName, timestamp, action) {
        return new Promise(function(resolve, reject){
            let queryString = `INSERT INTO \`${constants.databaseName}\`.\`${tableName}\`
                (\`timestamp\`, \`action\`)
                VALUES ('${timestamp}', '${action}')`

            db.query(queryString, function(err, rows, fields){
                if(err){
                    reject(err)
                }
                resolve(rows)
            })
        })
    }
} 

module.exports = {
    Queries
}