const db = require('../dbconnection')
const moment = require('moment');

const Queries = {
    insertNewHourlyTicker: function(databaseName, tableName, high, last, timestamp, bid, vwap, volume, low, ask, open){
        return new Promise(function(resolve, reject){

            let queryString = `INSERT INTO \`${databaseName}\`.\`${tableName}\` 
                            (\`high\`, \`last\`, \`timestamp\`, \`bid\`, \`vwap\`, \`volume\`, \`low\`, \`ask\`, \`open\`)
                            VALUES(${high}, ${last}, '${timestamp}', ${bid}, ${vwap}, ${volume}, ${low}, ${ask}, ${open})`

            db.query(queryString, function(err, rows, fields){
                if(err){
                    reject(err)
                }
                resolve(rows)
            })

        })
    }
} 

module.exports = {
    Queries
}