const db = require('../dbconnection')

const Queries = {
    createDatabase:function(databaseName){
        return new Promise(function(resolve, reject) {
            db.query("CREATE DATABASE IF NOT EXISTS "+databaseName+";", function(err, data){
                if(err){
                    reject(err)
                }
                resolve(data)
            })
        })
    },
    dropTable:function(databaseName, tableName){
        return new Promise(function(resolve, reject) {
            db.query("DROP TABLE IF EXISTS "+databaseName+"."+tableName+";", function(err, data){
                if(err){
                    reject(err)
                }
                resolve(data)
            })
        });
    },
    createTableHourlyTicker: function(databaseName, tableName){
        let queryString = "CREATE TABLE `"+databaseName+"`.`"+tableName+"` ( \
            `id` INT NOT NULL AUTO_INCREMENT, \
            `high` DECIMAL(15, 2) NOT NULL, \
            `last` DECIMAL(15, 2) NOT NULL, \
            `timestamp` TIMESTAMP NOT NULL, \
            `bid` DECIMAL(15, 2) NOT NULL, \
            `vwap` DECIMAL(15, 2) NOT NULL, \
            `volume` DECIMAL(15, 8) NOT NULL, \
            `low` DECIMAL(15, 2) NOT NULL, \
            `ask` DECIMAL(15, 2) NOT NULL, \
            `open` DECIMAL(15, 2) NOT NULL, \
            PRIMARY KEY (`id`), \
            UNIQUE INDEX `id_UNIQUE` (`id` ASC));"

        return new Promise(function(resolve, reject) {
            db.query(queryString, function(err, data){
                if(err){
                    reject(err)
                }
                resolve(data)
            })
        })
    },
    createTableMovingAverages: function(databaseName, tableName){
        let queryString = "CREATE TABLE `"+databaseName+"`.`"+tableName+"` ( \
            `id` INT NOT NULL AUTO_INCREMENT, \
            `timestamp` TIMESTAMP NOT NULL, \
            `sma` DECIMAL(15, 2) NULL,  \
            `ema1` DECIMAL(15, 2) NULL, \
            `ema2` DECIMAL(15, 2) NULL, \
            PRIMARY KEY (`id`), \
            UNIQUE INDEX `id_UNIQUE` (`id` ASC));"

        return new Promise(function(resolve, reject) {
            db.query(queryString, function(err, data){
                if(err){
                    reject(err)
                }
                resolve(data)
            })
        })
    },
    createTableTradingActions: function(databaseName, tableName){
        let queryString = "CREATE TABLE `"+databaseName+"`.`"+tableName+"` ( \
                `id` INT NOT NULL AUTO_INCREMENT, \
                `timestamp` TIMESTAMP NOT NULL, \
                `action` VARCHAR(1) NOT NULL, \
                PRIMARY KEY (`id`), \
                UNIQUE INDEX `id_UNIQUE` (`id` ASC));"

        return new Promise(function(resolve, reject) {
            db.query(queryString, function(err, data){
                if(err){
                    reject(err)
                }
                resolve(data)
            })
        })
    }       
}

module.exports = {
    Queries
}