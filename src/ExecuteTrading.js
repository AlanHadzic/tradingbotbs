const logger = require('../log/log')
const constants = require('../constants')
const configuration = require('../configuration.json')
const dc = require('./DataCollectorBS')
const request = require('request')
const crypto = require('crypto')
const nonce = require('nonce')() 
const querystring = require('querystring')

function generateApiSignature(currency) {
    const key = configuration[currency].key ? configuration[currency].key : ""
    const secret = configuration[currency].secret ? configuration[currency].secret : ""
    const userID = configuration.userID ? configuration.userID : ""

    let n = nonce()

    let hash = crypto.createHmac('sha256', secret)
        .update(n+userID+key)
        .digest('hex').toUpperCase()
    
    let form = {
        key: key,
        signature: hash,
        nonce: n
    }

    return form
}

function postRequest(uri, formData){
    return new Promise(function(resolve, reject){
        request({
            headers: {
              'Content-Length': formData.length,
              'Content-Type': 'application/x-www-form-urlencoded'
            },
            uri: uri,
            body: formData,
            method: 'POST'
          }, function (err, res, body) {
            if(err){
                reject(err)
            }else{
                resolve(res.body)
            }
        })
    })
}

async function getBalanceInfo(currency) {
    try {
        let form = generateApiSignature(currency)
        let formData = querystring.stringify(form)
        const uri = constants.balanceRequestUrl()
        let data = await postRequest(uri, formData)
        return data
    } catch (err) {
        logger.log.error(`Getting balance info for ${currency}. Error ${err}`)
        return null
    }
}

async function sell(amount, currency){
    try {
        let uri = constants.sellMarketURL(currency)
        let form = generateApiSignature(currency)
        form.amount = amount

        let formData = querystring.stringify(form)
        let data = await postRequest(uri, formData)
        logger.log.info(`SELL: ${amount} ${currency}.`)
        return data
    } catch (err) {
        logger.log.error(`SELL: ${amount} ${currency}. Error ${err}`)
    }
}

async function buy(amount, currency){
    try {
        let uri = constants.buyMarketURL(currency)
        let form = generateApiSignature(currency)
        form.amount = amount

        let formData = querystring.stringify(form)
        let data = await postRequest(uri, formData)
        logger.log.info(`BUY: ${amount} ${currency}`)
        return data
    } catch ( err ) {
        logger.log.error(`BUY: ${amount} ${currency}. Error ${err}`)
    }
}

async function executeTradingAction(crossOverChangeArr) {

    for ( let i in crossOverChangeArr ) {
        let currency = crossOverChangeArr[i][0]
        let action = crossOverChangeArr[i][1]
        
        logger.log.info(`Executing action - ${action}, on currency: ${currency}`)

        let blanceResponse = await getBalanceInfo(currency)
        if(!blanceResponse) continue
        let balance = JSON.parse(blanceResponse)  

        if(balance.status === 'error') {
            logger.log.error(`Getting balance info: ${JSON.stringify(balance)}`)
            continue
        }      
        let amount, prices, data

        if ( action == 's' ) {
            // SELL
            amount = balance[`${currency}_available`]
            if (amount > 0 ) {
                data = await sell(amount, currency)
                logger.log.info(JSON.stringify(data))
            }
        } else {
            // BUY
            amount = balance['eur_available']
            if ( amount > 0 ) {

                prices = await dc.getRequest(`${currency}eur`)
                
                let buyingPrice = (amount-(amount*0.01))/prices.last
                buyingPrice = parseInt(buyingPrice * 100000000)
                buyingPrice /= 100000000

                data = await buy( buyingPrice, currency )
                logger.log.info(JSON.stringify(data))
            }
        }
    } 
}

module.exports = {
    executeTradingAction
}