const logger = require('../log/log')
const constants = require('../constants')
const configuration = require('../configuration.json')
const movingAverages = require('../models/movingAverages_model')
const tradingAction = require('../models/tradingAction_model')
const executeTrading = require('./ExecuteTrading')
const dc = require('./DataCollectorBS')

function defineAction(row){
    let action = 'b'
    if(row[0].ema1 < row[0].ema2) {
        action = 's'
    }
    return action
}

async function defineTradingActions(tableNameTA, tableNameMA, currency, crossOverChangeArr) {
    
    let data
    let row = await movingAverages.Queries.getLastNrows(tableNameMA, 1)
    if (row.length === 0) return 

    let action = defineAction(row)
    let lastAction = await tradingAction.Queries.getLastTradingAction(tableNameTA)
    let timestamp = dc.timeConverter()

    try {
        if ( lastAction.length === 0 ) {

            data = await tradingAction.Queries.insertTradingAction(tableNameTA, timestamp, action)
            logger.log.info(`Storing detected crossover: ${tableNameTA}, action: ${action}`)
    
        } else if ( lastAction[0].action != action ) {
    
            data = await tradingAction.Queries.insertTradingAction(tableNameTA, timestamp, action)
            logger.log.info(`Storing detected crossover: ${tableNameTA}, action: ${action}`)
    
            if (configuration.tradingCurrency.indexOf(currency) != -1) {
                crossOverChangeArr.push([currency, action])
            }
    
        } 
    } catch (err) {
        logger.log.error(`Storing detected crossover: ${tableNameTA}, action: ${action}. Error: ${err}`)
    }
}

async function tradingActions() {
    
    let crossOverChangeArr = []
    let constantsArr = [
        [constants.taEthTableName, constants.maEthTableName, 'eth'],
        [constants.taBtcTableName, constants.maBtcTableName, 'btc'],
        [constants.taLtcTableName, constants.maLtcTableName, 'ltc'],
        [constants.taXrpTableName, constants.maXrpTableName, 'xrp'],
        [constants.taBchTableName, constants.maBchTableName, 'bch']
    ]
    let data

    for (var i in constantsArr) {
        let tableNameTA = constantsArr[i][0]
        let tableNameMA = constantsArr[i][1]
        let currency    = constantsArr[i][2]
        
        try {
            data = await defineTradingActions(tableNameTA, tableNameMA, currency, crossOverChangeArr)
        } catch (err) {
            logger.log.error(`Defining trading action. Error: ${err}`)
            continue
        }
    }

    if (crossOverChangeArr.length > 0 && configuration.trading) {
        data = await executeTrading.executeTradingAction(crossOverChangeArr)
    }
}

module.exports = {
    tradingActions
}