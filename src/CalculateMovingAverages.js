const logger = require('../log/log')
const constants = require('../constants')
const movingAverages = require('../models/movingAverages_model')
const dc = require('./DataCollectorBS')

async function calculateSMA(tableName, SMALength){

    let rows = await movingAverages.Queries.getLastNrows(tableName, SMALength)
   
    if(rows.length === SMALength){
        let sum = 0
        for ( let row of rows ) {
            sum += row.last
        }
        return sum/SMALength
    }
    return null
}

async function calculateEMA(tablePrices, tableEMA, EMALength){

    let lastEma = await movingAverages.Queries.getLastNrows(tableEMA, 1)

    let isEma1 = true
    let column = 'ema1'
    if (EMALength === constants.EMA2Length){
        isEma1 = false
        column = 'ema2'
    }

    let value = null

    if ( lastEma.length === 0 || !lastEma[0][column] ) {
        
        value = await calculateSMA(tablePrices, EMALength)

    } else {

        let lastPrice = await movingAverages.Queries.getLastNrows( tablePrices, 1)
        if(lastPrice.length === 1){

            let K = 2 / ( EMALength + 1 )
            if(isEma1){
                value = ((lastPrice[0].last - lastEma[0].ema1) * K) + lastEma[0].ema1
            }else{
                value = ((lastPrice[0].last - lastEma[0].ema2) * K) + lastEma[0].ema2
            }
        } 
    }
    return value
}


async function calculateMovingAverages() {

    let time = dc.timeConverter()

    let constantsArr = [
        [constants.ethTableName, constants.maEthTableName],
        [constants.btcTableName, constants.maBtcTableName],
        [constants.ltcTableName, constants.maLtcTableName],
        [constants.xrpTableName, constants.maXrpTableName],
        [constants.bchTableName, constants.maBchTableName]
    ]

    for (var i in constantsArr){
        let priceTable = constantsArr[i][0]
        let maTable = constantsArr[i][1]
        
        try {
            let sma = await calculateSMA(priceTable, constants.SMALength)
            let ema1 = await calculateEMA(priceTable, maTable, constants.EMA1Length)
            let ema2 = await calculateEMA(priceTable, maTable, constants.EMA2Length)

            if( sma ) {
                try {
                    let data = await movingAverages.Queries.insertMovingAverages(maTable, time, sma, ema1, ema2)
                    logger.log.info(`Calculate indicators and store into: ${maTable}`)
                }catch (err) {
                    logger.log.error(`Calculate indicators and store into: ${maTable}. Error: ${err}`)
                }
            }
        } catch (err) {
            logger.log.error(`Calculate indicators. Error: ${err}`)
            continue
        }
    }
}

module.exports = {
    calculateMovingAverages
}