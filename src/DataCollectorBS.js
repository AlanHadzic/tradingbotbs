const request = require('request')
const logger = require('../log/log')
const constants = require('../constants')
const hourlyTicker = require('../models/hourlyTicker_model')

function timeConverter(timestamp = 0){
    var a
    if (timestamp === 0) {
        a = new Date()
    }else {
        a = new Date(timestamp * 1000)
    }

    var year = a.getFullYear()
    var month = a.getMonth() + 1 
    var date = a.getDate()
    var hour = a.getHours()
    var min = a.getMinutes()
    var sec = a.getSeconds()
    var time = `${year}-${month}-${date} ${hour}:${min}:${sec}` 
    return time
 }

function getRequest(currencyPair){
    return new Promise(function(resolve, reject){
        let url = constants.hourlyTickerRequestURL(currencyPair)
        request(url, { json: true }, (err, response, body) => {
            if (err) { 
                reject(err)
            }
            resolve(response.body)
        });
    })
}

async function storeHourlyData(databaseName, tableName, currencyPair){
    try{
        var responseData = await getRequest(currencyPair)
        try{
            var timestampDB = timeConverter(responseData.timestamp)
            var data = await hourlyTicker.Queries.insertNewHourlyTicker(
                databaseName,
                tableName,
                responseData.high, 
                responseData.last, 
                timestampDB,
                responseData.bid, 
                responseData.vwap, 
                responseData.volume, 
                responseData.low, 
                responseData.ask, 
                responseData.open)
            logger.log.info(`${currencyPair} data successfully stored into ${tableName}`)
        }catch(err){
            logger.log.error(`Stroing ${currencyPair} data into ${tableName}`)
        }
    }catch(err){
        logger.log.error(`Executing get request ${currencyPair}: ${err}`)
    }
}

async function getPricesAndStoreToDB() {
    let [ ethData, bchData, btcData, ltcData, xrpData ] = await Promise.all([
        storeHourlyData(constants.databaseName, constants.ethTableName, constants.eth),
        storeHourlyData(constants.databaseName, constants.bchTableName, constants.bch),
        storeHourlyData(constants.databaseName, constants.btcTableName, constants.btc),
        storeHourlyData(constants.databaseName, constants.ltcTableName, constants.ltc),
        storeHourlyData(constants.databaseName, constants.xrpTableName, constants.xrp)
    ])
    logger.log.info("Data Collector process end")
}

module.exports = {
    getPricesAndStoreToDB,
    timeConverter,
    getRequest,
}