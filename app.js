const CronJob = require('cron').CronJob;
const dc = require('./src/DataCollectorBS')
const cma = require('./src/CalculateMovingAverages')
const ta = require('./src/TradingAction')
const logger = require('./log/log')

const job = new CronJob({
    cronTime: '42 * * * *',
    onTick: async function() {

        logger.log.info(">>> COLLECTING DATA")
        let collectingData = await dc.getPricesAndStoreToDB()

        logger.log.info(">>> CALCULATE INDICATORS")
        let calculateIndicators = await cma.calculateMovingAverages()

        logger.log.info(">>> TRADING")
        let tradingAction = await ta.tradingActions()

    },
    start: true,
    timeZone: 'Europe/Ljubljana'
})

console.log('starting server')
// TODO